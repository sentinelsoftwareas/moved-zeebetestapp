﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Zeebe.Client;
using Zeebe.Client.Api.Responses;
using Zeebe.Client.Api.Worker;
using ZeebeTest.Hub;

namespace ZeebeTest.HostedServices
{
    public class ZeebeWorkers : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IZeebeClient _client;
        private IServiceProvider _sp;

        public ZeebeWorkers(IConfiguration configuration, ILogger<ZeebeWorkers> logger, IServiceProvider sp)
        {
            _configuration = configuration;
            _logger = logger;
            _client = createClient();
            _sp = sp;
        }
        protected override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is starting.");

            Task.Run(() => StartCreatePreCalcWorker("action-create-pre-calc", "Test", HandleJob), cancellationToken: cancellationToken);

            return Task.CompletedTask;
        }

        private void StartCreatePreCalcWorker(string jobType, string workerName, JobHandler jobHandler)
        {
            using (var signal = new EventWaitHandle(false, EventResetMode.AutoReset, jobType))
            {
                _client.NewWorker()
                    .JobType(jobType)
                    .Handler(jobHandler)
                    .MaxJobsActive(5)
                    .Name(workerName)
                    .AutoCompletion()
                    .PollInterval(TimeSpan.FromSeconds(1))
                    .Timeout(TimeSpan.FromSeconds(10))
                    .Open();

                // blocks main thread, so that worker can run
                signal.WaitOne();
            }
        }
        private async void HandleJob(IJobClient jobClient, IJob job)
        {
            using (var scope = _sp.CreateScope())
            {
                var hubContext = scope.ServiceProvider.GetService<IHubContext<MessagesHub>>();
                // business logic
                var jobKey = job.Key;
                _logger.LogInformation("Handling job: " + job);
                await hubContext.Clients.All.SendAsync("addNewMessageToPage", new { message = $"Handling job:{job}" });
                //if (jobKey % 3 == 0)
                //{
                await jobClient.NewCompleteJobCommand(jobKey).Variables(JsonConvert.SerializeObject(new { JobTask = Guid.NewGuid() })).Send();
                //}
                //else if (jobKey % 2 == 0)
                //{
                //    jobClient.NewFailCommand(jobKey).Retries(job.Retries - 1).ErrorMessage("Example fail").Send();
                //}
                //else
                //{
                //    // auto completion
                //}
            }
        }
        private IZeebeClient createClient()
        {
            var zeebeUrl = _configuration["ZeebeUrl"];
            var client = ZeebeClient.Builder().UseGatewayAddress(zeebeUrl).UsePlainText().Build();
            return client;
        }

    }
}
