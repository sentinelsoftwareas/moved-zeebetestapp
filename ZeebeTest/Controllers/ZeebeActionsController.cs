﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Zeebe.Client;
using Zeebe.Client.Api.Responses;
using Zeebe.Client.Api.Worker;
using ZeebeTest.Hub;

namespace ZeebeTest.Controllers
{
    [ApiController]
    public class ZeebeActionsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ZeebeActionsController> _logger;
        private readonly IHubContext<MessagesHub> _hubContext;

        public ZeebeActionsController(IConfiguration configuration, ILogger<ZeebeActionsController> logger, IHubContext<MessagesHub> hubContext)
        {
            _configuration = configuration;
            _logger = logger;
            _hubContext = hubContext;
        }
        [HttpPost("api/ZeebeActions/CreateFlow/{registrationNumber}")]
        public async Task<IActionResult> CreateFlow([FromRoute] string registrationNumber, [FromBody] object @event)
        {
            // deploy
            var client = createClient();
            var variables = new
            {
                registrationNumber = registrationNumber
            };

            var workflowInstance = await client
                .NewCreateWorkflowInstanceCommand().BpmnProcessId(_configuration["ZeebeProcessId"]).LatestVersion()
                .Variables(JsonConvert.SerializeObject(@event))
                .Send();

            await _hubContext.Clients.All.SendAsync("addNewMessageToPage", new {message=$"Started Flow for registration number {registrationNumber}," +
                                 $"<br/>instance key {workflowInstance.WorkflowInstanceKey}" +
                                 $"<br/>WorkflowKey key {workflowInstance.WorkflowKey}" +  
                                 $"<br/>BpmnProcessId {workflowInstance.BpmnProcessId}"
            });
            return Ok($"Started Flow for registration number {registrationNumber}");
        }
        [HttpPost("api/ZeebeActions/{eventName}/{registrationNumber}/SendEvent")]
        public async Task<IActionResult> SendEvent([FromRoute] string eventName, [FromRoute] string registrationNumber, [FromBody] object @event)
        {

            await createClient()
                .NewPublishMessageCommand()
                .MessageName(eventName)
                .CorrelationKey(registrationNumber)
                .Variables(JsonConvert.SerializeObject(@event))
                .Send();
            
           // await _hubContext.Clients.All.SendAsync("addNewMessageToPage", new {message=$"Sent event {eventName} for registration number {registrationNumber}" });
            return Ok($"Sent event {eventName} for registration number {registrationNumber}");
        }
        [HttpPost("api/ZeebeDeploy/{fileName}")]
        public async Task<IActionResult> RunWorker([FromForm] IFormFile file, [FromRoute] string fileName )
        {
            var fileStream = file.OpenReadStream();
            try
            {
                var response = await createClient().NewDeployCommand().AddResourceStream(fileStream, fileName).Send();
                _logger.LogInformation($"Deployed file {fileName}, bytes {fileStream.Length}, key {response.Key}");
                return Ok(new
                {
                    Message= $"Deployed file {fileName}, bytes {fileStream.Length}, key {response.Key}",
                    File = new {
                        FileName = fileName,
                        FileLength = fileStream.Length
                    },
                    WorkflowKey = response.Key,
                    Workflows = response.Workflows
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Exception {e.GetType().Name} message: {e.Message} occured");
                throw;
            }
            
            
            return Ok($"Deployed file {fileName}, bytes {fileStream.Length}");
        }
        [HttpPost("api/ZeebeActions/RunWorker/{jobType}/")]
        public async Task<IActionResult> RunWorker([FromRoute] string jobType, [FromRoute] string registrationNumber)
        {
            var  cts = new CancellationTokenSource();;
            cts.CancelAfter(2000);
            await Task.Run(() => StartCreatePreCalcWorker(jobType, "ControllerWorker", HandleJob), cts.Token);

            return Ok($"Worker ran");
        }
        [HttpGet("api/ZeebeActions/Topology")]
        public async Task<IActionResult> GetTopology()
        {
            var x = await createClient().TopologyRequest().Send();
            
            return Ok(x);
        }

        private void StartCreatePreCalcWorker(string jobType, string workerName, JobHandler jobHandler)
        {
            using (var signal = new EventWaitHandle(false, EventResetMode.AutoReset))
            {
                createClient().NewWorker()
                    .JobType(jobType)
                    .Handler(jobHandler)
                    .MaxJobsActive(5)
                    .Name(workerName)
                    .AutoCompletion()
                    .PollInterval(TimeSpan.FromSeconds(1))
                    .Timeout(TimeSpan.FromSeconds(10))
                    .Open();


                // blocks main thread, so that worker can run
                signal.WaitOne();
            }
        }
        private async void HandleJob(IJobClient jobClient, IJob job)
        {
            // business logic
            var jobKey = job.Key;
            _logger.LogInformation("Handling job: " + job);
            await _hubContext.Clients.All.SendAsync("addNewMessageToPage",new { message = $"Handling job:{job}"});
            //if (jobKey % 3 == 0)
            //{
            await jobClient.NewCompleteJobCommand(jobKey).Variables(JsonConvert.SerializeObject(new {JobTask=Guid.NewGuid()})).Send();
            //}
            //else if (jobKey % 2 == 0)
            //{
            //    jobClient.NewFailCommand(jobKey).Retries(job.Retries - 1).ErrorMessage("Example fail").Send();
            //}
            //else
            //{
            //    // auto completion
            //}
        }

        private IZeebeClient createClient()
        {
            var zeebeUrl = _configuration["ZeebeUrl"];
            var client = ZeebeClient.Builder().UseGatewayAddress(zeebeUrl).UsePlainText().Build();
            return client;
        }
    }
}