﻿var zeebe = zeebe ? zeebe : {};
zeebe.createFlow = () => {
    registrationNumber = $("#regnummer").val();
    var response = {};


    $.ajax({
        type: "POST",
        url: "/api/ZeebeActions/CreateFlow/" + registrationNumber,
        data: JSON.stringify(response),
        success: zeebe.log("Success POST til /api/ZeebeActions/CreateFlow/" + registrationNumber),
        contentType: "application/json; charset=utf-8"
    });
};
zeebe.enterPrice = () => {
    registrationNumber = $("#regnummer").val();

    var response = {};
    response.price = $("#pris").val();
    var eventName = "message-price-entered"
    var uri = "/api/ZeebeActions/" + eventName + "/" + registrationNumber + "/SendEvent";
    $.ajax({
        type: "POST",
        url: uri,
        data: JSON.stringify(response),
        success: zeebe.log("Success POST til " + uri),
        contentType: "application/json; charset=utf-8"
    });
};
zeebe.approvedPrice = () => {
    registrationNumber = $("#regnummer").val();

    var response = {};


    var eventName = "message-price-approved";
    var uri = "/api/ZeebeActions/" + eventName + "/" + registrationNumber + "/SendEvent";
    $.ajax({
        type: "POST",
        url: uri,
        data: JSON.stringify(response),
        success: zeebe.log("Success POST til " + uri),
        contentType: "application/json; charset=utf-8"
    });
};
zeebe.runAction = (action) => {

    var response = {};

    $.ajax({
        type: "POST",
        url: "/api/ZeebeActions/RunWorker/" + action,
        data: JSON.stringify(response),
        success: zeebe.log("Post action /api/ZeebeActions/RunWorker/" + action),
        contentType: "application/json; charset=utf-8"
    });
};
zeebe.log = (message) => {
    $('#messages').prepend('<div style="background-color: white;>' +
        message +
        '</div>');
}