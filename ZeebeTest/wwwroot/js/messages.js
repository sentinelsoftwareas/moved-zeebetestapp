﻿//< !--Script references. -- >
//  < !--The jQuery library is required and is referenced by default in _Layout.cshtml. -- >
//  < !--Reference the SignalR
//library. -- >

(() => {
    // Reference the auto-generated proxy for the hub.
    // Create a function that the hub can call back to display messages.

    var connection = new signalR.HubConnectionBuilder().withUrl("/messagesHub").build();
    var startSignalRConnection = connection => connection.start()
        .then(() => console.info('Websocket Connection Established'))
        .catch(err => console.error('SignalR Connection Error: ', err));
    connection.onclose(() => setTimeout(startSignalRConnection(connection), 5000));


    connection.on("addNewMessageToPage",
        function(message) {
            var date = new Date();
            var dateString = pad(date.getDate(), 2) +
                "." +
                pad(date.getMonth(), 2) +
                "." +
                date.getFullYear() +
                " " +
                pad(date.getHours(), 2) +
                ":" +
                pad(date.getMinutes(), 2)+
                ":" +
                pad(date.getSeconds(), 2);
            
            // Add the message to the page.
            $('#messages').prepend(
                '<div class="message-container">' +
                '<div class="message-date">' + dateString + '</div>' +
                '<div class="message-message">' +
                message.message +
                '</div></div>');
            $('#no-messages').html("");
        });

    // Start the connection.
    connection.start().catch(function(err) {
        return console.error(err.toString());
    });
})();

function pad(val, width) {
    var z = "0";
    val = val + "";
    return val.length >= width ? val : new Array(width - val.length + 1).join(z) + val;
}

// This optional function html-encodes messages for display in the page.
function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}

function syntaxHighlight(json) {
    try {
        if (typeof json !== 'string') {
            json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');

        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }

            return '<span class="' + cls + '">' + match + '</span>';
        });
    }
    catch (err) {
        return json;
    }
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function calculateAndSetTime(d, timerGuidId) {
    var message = calculateTimeDif(d);
    var divId = "#" + timerGuidId;
    $(divId).html(message);
}

function calculateTimeDif(d) {
    var now = new Date();
    var timeDiff = Math.round((now.getTime() - d.getTime()) / 1000);
    if (timeDiff < 60) return "Now";
    timeDiff = Math.round(timeDiff / 60);
    if (timeDiff < 60) return " " + timeDiff + " minutter siden";
    timeDiff = Math.round(timeDiff / 60);
    return " " + timeDiff + " timer siden";
}