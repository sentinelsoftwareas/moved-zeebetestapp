﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace ZeebeTest.Hub
{
    public class MessagesHub: Microsoft.AspNetCore.SignalR.Hub
    {
        public async Task SendMessage(MessagesSendModel message)
        {
            // Call the addNewMessageToPage method to update clients.
            await Clients.All.SendAsync("addNewMessageToPage", message);
        }
    }
    public class MessagesSendModel
    {
        public DateTime ReceivedTime { get; set; } = DateTime.Now;
        public string Message { get; set; }
    }
}
